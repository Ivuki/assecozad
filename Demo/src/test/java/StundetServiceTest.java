import exception.StudentException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import repository.StudentRepo;
import repository.impl.StudentRepoImpl;
import service.CsvService;
import service.StudentService;
import service.impl.CsvServiceImpl;
import service.impl.StudentServiceImpl;

import java.io.IOException;

public class StundetServiceTest {

    @Test
    @DisplayName("Should not create with bad JMBAG")
    public void shouldNotCreateWithBadJMBAG() throws IOException {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        csvService.loadToMemory();
        Assertions.assertAll(
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("0036577233", "Igor", "Vukas", "5")), //exists
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("00365772aa", "Igor", "Vukas", "5")), //has letters
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("00360228", "Igor", "Vukas", "5")), //to short
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("0036412380228", "Igor", "Vukas", "5")) //to long
        );
    }

    @Test
    @DisplayName("Should not create with bad name")
    public void shouldNotCreateWithBadName() {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        Assertions.assertAll(
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("0036480228", "I<>gor", "Vukas", "5")), //bad chars
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("0036480228", "Igor", "Vu12kas", "5")) //letters
        );
    }

    @Test
    @DisplayName("Should not create with bad Grade")
    public void shouldNotCreateWithBadGrade() {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        Assertions.assertAll(
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("0036480228", "Igor", "Vukas", "0")), //to low
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("0036480228", "Igor", "Vukas", "12")), //to high
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.create("0036480228", "Igor", "Vukkas", "a")) //letter
        );
    }

    @Test
    @DisplayName("Should create student")
    public void shouldCreateStudent() throws IOException {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        csvService.loadToMemory();
        Assertions.assertDoesNotThrow(() -> studentService.create("0036480228", "Igor", "Vukas", "4"));
    }

    @Test
    @DisplayName("Should not read if jmbag doesn't exist")
    public void shouldNotReadIfJmbagDoesNotExist() throws IOException {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        csvService.loadToMemory();
        Assertions.assertThrows(StudentException.class, () -> studentService.read("0000")); //jmbag doesn't exist
    }

    @Test
    @DisplayName("Should read")
    public void shouldRead() throws IOException {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        csvService.loadToMemory();
        Assertions.assertDoesNotThrow(() -> studentService.read("0036577233"));
    }

    @Test
    @DisplayName("Should not filter grade with bad grade")
    public void shouldNotFilterGradeWithBadGrade() {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        Assertions.assertAll(
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterGrade("-1", "e")), //under 0
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterGrade("12", "e")), //over 9
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterGrade("X", "e")) // letter
        );

    }

    @Test
    @DisplayName("Should not filter grade with bad type")
    public void shouldNotFilterGradeWithBadType() {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        Assertions.assertAll(
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterGrade("5", "x")), //not e l g
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterGrade("5", "0")), //not letter
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterGrade("5", "-e")) //has -
        );
    }

    @Test
    @DisplayName("Should filter grade")
    public void shouldFilterGrade() throws IOException {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        csvService.loadToMemory();
        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> studentService.filterGrade("0", "g")),
                () -> Assertions.assertDoesNotThrow(() -> studentService.filterGrade("9", "l")),
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterGrade("0", "e"))
        );
    }

    @Test
    @DisplayName("Should not filter Name with bad type")
    public void shouldNotFilterNameWithBadType() {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        Assertions.assertAll(
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterName("p", "x")), // bad letter
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterName("p", "0")), // number
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterName("p", "-e")) // doent exist
        );
    }

    @Test
    @DisplayName("Should filter Name")
    public void shouldFilterName() throws IOException {
        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl("test.csv", studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);
        csvService.loadToMemory();
        Assertions.assertAll(
                () -> Assertions.assertThrows(StudentException.class, () -> studentService.filterName("2")),
                () -> Assertions.assertDoesNotThrow(() -> studentService.filterName("a", "-u")),
                () -> Assertions.assertDoesNotThrow(() -> studentService.filterName("a", "-l")),
                () -> Assertions.assertDoesNotThrow(() -> studentService.filterName("a"))
        );
    }
}
