import controller.StudentController;
import repository.StudentRepo;
import repository.impl.StudentRepoImpl;
import service.CsvService;
import service.StudentService;
import service.impl.CsvServiceImpl;
import service.impl.StudentServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Demo {

    public static void main(String[] args) throws IOException {

        StudentRepo studentRepo = new StudentRepoImpl();
        CsvService csvService = new CsvServiceImpl(args[0], studentRepo);
        StudentService studentService = new StudentServiceImpl(csvService, studentRepo);

        csvService.loadToMemory();

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in)
        );

        new StudentController(studentService, reader);

        reader.close();
    }
}
