package controller;

import exception.StudentException;
import service.StudentService;

import java.io.BufferedReader;
import java.io.IOException;

public class StudentController {

    public final StudentService studentService;
    public final BufferedReader reader;

    public StudentController(StudentService studentService, BufferedReader reader) throws IOException {
        this.studentService = studentService;
        this.reader = reader;

        start();
    }

    public void start() throws IOException {

        System.out.println("Dobrodosli!" +
                " Naredbe koje su vam na raspolaganju su: \n" +
                "create jbmag ime prezime ocjena \n" +
                "read jbmag \n" +
                "filter-grade ocjena relacija(l,e,g) \n" +
                "filter-name pocetak \n" +
                "exit \n" +
                ">> "
        );

        while (true) {
            String line = reader.readLine();
            String[] inputs = line.split(" ");

            if (inputs[0].equals("exit")) break;

            try {
                switch (inputs[0]) {
                    case "create":
                        create(inputs);
                        break;
                    case "read":
                        read(inputs);
                        break;
                    case "filter-grade": {
                        filterGrade(inputs);
                        break;
                    }
                    case "filter-name": {
                        filterName(inputs);
                        break;
                    }
                    default:
                        throw new StudentException("Nepoznata naredba! Postojece naredbe su: create, read, filter-grade, filter-name, exit");
                }
            } catch (StudentException e) {
                System.out.println(e.getMessage());
            }
            System.out.println(">>");
        }
    }

    public void create(String[] inputs) {
        Thread.UncaughtExceptionHandler h = (th, e) -> System.out.println(e.getMessage());
        Thread t = new Thread(() -> {
            if (inputs.length != 5) {
                throw new StudentException("Pogresan broj parametara");
            } else {
                try {
                    if (studentService.create(inputs[1], inputs[2], inputs[3], inputs[4])) {
                        System.out.println("Student uspjesno stvoren!");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.setUncaughtExceptionHandler(h);
        t.start();
    }

    public void read(String[] inputs) {
        Thread.UncaughtExceptionHandler h = (th, e) -> System.out.println(e.getMessage());
        Thread t = new Thread(() -> {
            if (inputs.length != 2) {
                throw new StudentException("Pogresan broj parametara");
            } else {
                System.out.println(studentService.read(inputs[1]).toString());
            }
        });
        t.setUncaughtExceptionHandler(h);
        t.start();
    }

    public void filterGrade(String[] inputs) {
        Thread.UncaughtExceptionHandler h = (th, e) -> System.out.println(e.getMessage());
        Thread t = new Thread(() -> {
            if (inputs.length != 3) {
                throw new StudentException("Pogresan broj parametara");
            } else {
                studentService.filterGrade(inputs[1], inputs[2]).forEach(System.out::println);
            }
        });
        t.setUncaughtExceptionHandler(h);
        t.start();
    }

    public void filterName(String[] inputs) {
        Thread.UncaughtExceptionHandler h = (th, e) -> System.out.println(e.getMessage());
        Thread t = new Thread(() -> {
            if (inputs.length == 2) {
                studentService.filterName(inputs[1]).forEach(System.out::println);
            } else if (inputs.length == 3) {
                studentService.filterName(inputs[1], inputs[2]).forEach(System.out::println);
            } else {
                throw new StudentException("Pogresan broj parametara");
            }
        });
        t.setUncaughtExceptionHandler(h);
        t.start();
    }
}
