package service;

import exception.StudentException;
import model.Student;

import java.io.IOException;
import java.util.List;

public interface StudentService {
    boolean create(String jmbag, String fName, String lName, String grade) throws IOException, StudentException;

    Student read(String jmbag) throws StudentException;

    List<Student> filterGrade(String grade, String type) throws StudentException;

    List<String> filterName(String vid) throws StudentException;

    List<String> filterName(String reg, String type) throws StudentException;
}
