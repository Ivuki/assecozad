package service;

import java.io.IOException;

public interface CsvService {
    void loadToMemory() throws IOException;

    void writeToFile(String[] data) throws IOException;
}
