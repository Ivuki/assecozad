package service.impl;

import model.Student;
import repository.StudentRepo;
import service.CsvService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class CsvServiceImpl implements CsvService {

    private final String csvPath;
    private final StudentRepo studentRepo;

    public CsvServiceImpl(String csvPath, StudentRepo studentRepo) {
        this.csvPath = csvPath;
        this.studentRepo = studentRepo;
    }

    @Override
    public void loadToMemory() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(this.csvPath))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.isEmpty() || line.isBlank()) return;
                String[] values = line.split(";");
                Student student = new Student(values[0], values[1], values[2], Byte.parseByte(values[3]));
                studentRepo.create(student);
            }
        }
    }

    @Override
    public void writeToFile(String[] data) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(this.csvPath, true));
        String line = String.join(";", data);
        line += "\n";
        writer.write(line);

        writer.close();
    }
}
