package service.impl;

import exception.StudentException;
import model.Student;
import repository.StudentRepo;
import service.CsvService;
import service.StudentService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService {

    private final StudentRepo studentRepo;
    private final CsvService csvService;

    public StudentServiceImpl(CsvService csvService, StudentRepo studentRepo) {
        this.csvService = csvService;
        this.studentRepo = studentRepo;
    }

    public boolean create(String jmbag, String fName, String lName, String grade) throws IOException, StudentException {

        this.validate(jmbag, fName, lName, grade);

        byte tGrade = Byte.parseByte(grade);
        Student student = new Student(jmbag, fName, lName, tGrade);
        studentRepo.create(student);

        String[] data = {jmbag, fName, lName, String.valueOf(tGrade)};
        csvService.writeToFile(data);

        return true;
    }

    public Student read(String jmbag) throws StudentException {
        Student student = studentRepo.getByJmbag(jmbag)
                .orElse(null);
        if (student == null) {
            throw new StudentException("Student s danim JMBAGom ne postoji u bazi");
        }
        return student;
    }

    public List<Student> filterGrade(String grade, String type) throws StudentException {
        if (!grade.chars().allMatch(Character::isDigit) || grade.length() > 1) {
            throw new StudentException("Ocijena mora biti broj 0-9");
        }
        byte tGrade = Byte.parseByte(grade);

        if (!(type.equals("l") || type.equals("e") || type.equals("g"))) {
            throw new StudentException("Nepoznati parametar: " + type);
        }

        List<Student> students = studentRepo.getByGrade(tGrade, type);
        if (students.size() == 0) {
            throw new StudentException("Ne postoji niti jedan student koji zadovoljava uvijete");
        }
        return students;
    }

    public List<String> filterName(String reg) throws StudentException {
        List<Student> students = studentRepo.getByName(reg);
        ArrayList<String> names = new ArrayList<>();
        if (students.size() > 0) {
            students.forEach(student -> names.add((student.getfName() + " " + student.getlName())));
        } else {
            throw new StudentException("Ne postoji niti jedan student koji zadovoljava uvijete");
        }
        return names;
    }

    public List<String> filterName(String reg, String type) throws StudentException {
        List<String> names = filterName(reg);
        if (type.equals("-u")) {
            names = names.stream().map(String::toUpperCase).collect(Collectors.toList());
        } else if (type.equals("-l")) {
            names = names.stream().map(String::toLowerCase).collect(Collectors.toList());
        } else {
            throw new StudentException("Nepoznati parametar: " + type);
        }
        return names;
    }

    private void validate(String jmbag, String fName, String lName, String grade) throws StudentException {
        if (!jmbag.chars().allMatch(Character::isDigit) || jmbag.length() != 10) {
            throw new StudentException("Nepravilan JMBAG! Jmbag mora biti duljine 10 i sadrzavati samo bojeve\"");
        }
        if (studentRepo.getByJmbag(jmbag).orElse(null) != null) {
            throw new StudentException("Student s danim JMBAGom vec postoji u bazi");
        }
        if (!fName.chars().allMatch(Character::isLetter)) {
            throw new StudentException("Nepravilno Ime! Ime smije sadrzavati samo slova");
        }
        if (!lName.chars().allMatch(Character::isLetter)) {
            throw new StudentException("Nepravilno Prezime! Ime smije sadrzavati samo slova");
        }
        if (!grade.chars().allMatch(Character::isDigit) || grade.length() != 1 || grade.charAt(0) < '1' || grade.charAt(0) > '5') {
            throw new StudentException("Nepravilna ocjena! Ocijena mora biti broj od 1 do 5");
        }
    }
}
