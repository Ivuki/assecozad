package repository.impl;

import model.Student;
import repository.StudentRepo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StudentRepoImpl implements StudentRepo {

    private static List<Student> DB = Collections.synchronizedList(new ArrayList<>());

    public StudentRepoImpl() {
    }

    @Override
    public void create(Student student) {
        DB.add(student);
    }

    @Override
    public Optional<Student> getByJmbag(String jmbag) {
        return DB.stream()
                .filter(student -> student.getJmbag().equals(jmbag))
                .findFirst();
    }

    @Override
    public List<Student> getByGrade(byte grade, String type) {
        switch (type) {
            case "g":
                return DB.stream()
                        .filter(student -> (student.getGrade() > grade))
                        .collect(Collectors.toList());
            case "l":
                return DB.stream()
                        .filter(student -> (student.getGrade() < grade))
                        .collect(Collectors.toList());
            default:
                return DB.stream()
                        .filter(student -> (student.getGrade() == grade))
                        .collect(Collectors.toList());
        }
    }

    @Override
    public List<Student> getByName(String reg) {
        return DB.stream()
                .filter(student ->
                        student.getfName().startsWith(reg.toLowerCase())
                                || student.getfName().startsWith(reg.toUpperCase()))
                .collect(Collectors.toList());
    }
}
