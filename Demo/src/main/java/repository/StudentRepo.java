package repository;

import model.Student;

import java.util.List;
import java.util.Optional;

public interface StudentRepo {
    void create(Student student);

    Optional<Student> getByJmbag(String jmbag);

    List<Student> getByGrade(byte grade, String type);

    List<Student> getByName(String reg);
}
