package model;

import java.util.Objects;

public class Student {

    private static long IDCounter = 0;

    private long id;
    private String jmbag;
    private String fName;
    private String lName;
    private byte grade;

    public Student(String jmbag, String fName, String lName, byte grade) {
        this.id = ++IDCounter;
        this.jmbag = jmbag;
        this.fName = fName;
        this.lName = lName;
        this.grade = grade;
    }

    public long getId() {
        return id;
    }

    public String getJmbag() {
        return jmbag;
    }

    public void setJmbag(String jmbag) {
        this.jmbag = jmbag;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public byte getGrade() {
        return grade;
    }

    public void setGrade(byte grade) {
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id &&
                jmbag.equals(student.jmbag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, jmbag);
    }

    @Override
    public String toString() {
        return "{" +
                "jmbag: '" + jmbag + '\'' +
                ", ime: '" + fName + '\'' +
                ", prezime: '" + lName + '\'' +
                ", ocjena: " + grade +
                '}';
    }

}
