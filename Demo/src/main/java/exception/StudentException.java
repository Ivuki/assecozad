package exception;

public class StudentException extends RuntimeException {
    public StudentException(String message) {
        super("Greska! " + message);
    }
}
